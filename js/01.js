 class Machine {
    constructor (){
        this.turn = false
    };
    turnOn (){
       this.turn = true;
       console.log('Кнопка питания нажата.');
       console.log('Машина готова к работе.');
   };

   turnOff (){
       this.turn = false;
       console.log('Машина завершила работу.');
    };
 }

 class HomeAppliance extends Machine {
    constructor (){
        super();
        this.pluged = false;
    }

    plugIn (){
        this.pluged = true;
        console.log('Включена в сеть!');
    };

    plugOff (){
        this.pluged = false;
        console.log('Отключена от сети!');
    };

 }

 class LightSource extends HomeAppliance {
     constructor (){
         super();
         this.level = 0;
     }

     setLevel (level){
         if (level){
             this.level = level;
             console.log('Уровень освещенности: ' + level);
         } else console.log("установите уровень освещения");
     };
 }

 class WashingMachine extends HomeAppliance {
    constructor(){
        super();
    }
    turnOn (){
        if (this.pluged){
            this.turn = true;
            console.log('Кнопка питания нажата.');
        }
    };
    run (){
        if (this.pluged && this.turn) console.log("Машина работает.");
        else console.log("Машина не подключена к сети.");
    };
 }

 class AutoVehicle extends Machine {
     constructor() {
         super();
         this.position = {
             x: 0,
             y: 0
         };
     }

     setPosition (x, y) {
         this.position.x = x;
         this.position.y = y;
         console.log('Координаты: ' + 'x: ' + this.position.x + ',' + ' ' + 'y: ' + this.position.y);
     };
 }

 class Car extends AutoVehicle {
     constructor(){
         super();
         this.speed = 10;
     }

     setSpeed (speed) {
         this.speed = speed;
         console.log('Скорость автомобиля: ' + speed + ' ' + 'км/ч');
     };

     run (x, y) {
         let interval = setInterval( () => {
             let newX = this.position.x + this.speed;
             let newY = this.position.y + this.speed;
             if (newX >= x)  newX = x;
             if (newY >= y)  newY = y;
             this.setPosition(newX, newY);

             if(newX === x && newY === y) {
                 console.log('Пункт назначения достигнут.');
                 clearInterval(interval);
             }
         }, 1000);
     };
 }



